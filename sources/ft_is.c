/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 12:33:00 by dkovalch          #+#    #+#             */
/*   Updated: 2017/07/23 19:47:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

bool	ft_isprint(const int letter)
{
	return (letter >= 0x20 && letter <= 0x7E);
}

bool	ft_isascii(const int letter)
{
	return (letter >= 0 && letter <= 127);
}

bool	ft_isalnum(const int letter)
{
	return (ft_isalpha(letter) || ft_isdigit(letter));
}

bool	ft_isalpha(const int letter)
{
	return ((letter >= 'a' && letter <= 'z') ||
			(letter >= 'A' && letter <= 'Z'));
}

bool	ft_isdigit(const int letter)
{
	return (letter >= '0' && letter <= '9');
}
