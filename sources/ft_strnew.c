/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 15:14:13 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/26 20:32:49 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	*ft_strnew(size_t size)
{
	return ((char*)ft_memalloc(++size));
}

void	ft_strdel(char **as)
{
	ft_memdel((void**)as);
}

void	ft_strclr(char *const str)
{
	ft_bzero((void*)str, ft_strlen(str));
}

char	*ft_strdup(const char *const str)
{
	return (ft_strjoin(str, ""));
}

char	*ft_strsub(char const *const str, const size_t start, const size_t len)
{
	char	*rt;

	if (str == NULL || !(rt = ft_strnew(len)))
		return (NULL);
	return (ft_strncpy(rt, &str[start], len));
}
