/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/28 19:33:46 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:21:11 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

ssize_t	ft_putwstr_fd(const wchar_t *const str, const int fd)
{
	ssize_t ct;

	ct = 0;
	while (str != NULL && str[ct] != '\0')
		ft_putchar_fd(str[ct++], fd);
	return (ct);
}

ssize_t	ft_putwstr(const wchar_t *const str)
{
	return (ft_putwstr_fd(str, STDOUT_FILENO));
}
