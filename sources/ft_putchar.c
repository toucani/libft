/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 16:44:11 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/25 22:18:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#define BIT7 127
#define BIT11 2047
#define BIT16 65535
#define BIT21 2097151
#define BIT26 67108863
#define BIT31 2147483647

/*
**	UTF8 Unicode translator
*/

static void		ft_putchar_fd_2(const uint32_t symbol, char *rt)
{
	if (symbol <= BIT21)
	{
		rt[0] = ((symbol & 1835008) >> 18) | 240;
		rt[1] = ((symbol & 258048) >> 12) | 128;
		rt[2] = ((symbol & 4032) >> 6) | 128;
		rt[3] = (symbol & 63) | 128;
	}
	else if (symbol <= BIT26)
	{
		rt[0] = ((symbol & 50331648) >> 24) | 248;
		rt[1] = ((symbol & 16515072) >> 18) | 128;
		rt[2] = ((symbol & 258048) >> 12) | 128;
		rt[3] = ((symbol & 4032) >> 6) | 128;
		rt[4] = (symbol & 63) | 128;
	}
	else
	{
		rt[0] = ((symbol & 1073741824) >> 30) | 252;
		rt[1] = ((symbol & 1056964608) >> 24) | 128;
		rt[2] = ((symbol & 16515072) >> 18) | 128;
		rt[3] = ((symbol & 258048) >> 12) | 128;
		rt[4] = ((symbol & 4032) >> 6) | 128;
		rt[5] = (symbol & 63) | 128;
	}
}

ssize_t			ft_putchar_fd(const uint32_t symbol, const int fd)
{
	char		*rt;
	ssize_t		ct;

	rt = ft_strnew(6);
	if (symbol <= BIT7)
		rt[0] = symbol;
	else if (symbol <= BIT11)
	{
		rt[0] = ((symbol & 1984) >> 6) | 192;
		rt[1] = (symbol & 63) | 128;
	}
	else if (symbol <= BIT16)
	{
		rt[0] = ((symbol & 61440) >> 12) | 224;
		rt[1] = ((symbol & 4032) >> 6) | 128;
		rt[2] = (symbol & 63) | 128;
	}
	else
		ft_putchar_fd_2(symbol, rt);
	ct = write(fd, rt, sizeof(char)) + write(fd, &rt[1], ft_strlen(&rt[1]));
	ft_strdel(&rt);
	return (ct);
}

ssize_t			ft_putchar(const uint32_t symbol)
{
	return (ft_putchar_fd(symbol, STDOUT_FILENO));
}
