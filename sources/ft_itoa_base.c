/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/09 12:05:26 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:19:36 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

static void	ft_uitoa_base_r(const uintmax_t nmb, const unsigned int base,
							const char base_case, char *mem)
{
	if (nmb < base)
	{
		if (nmb < 10)
			mem[ft_strlen(mem)] = '0' + nmb;
		else
			mem[ft_strlen(mem)] =
				((base_case >= 'a' && base_case <= 'z') ? 'a' : 'A') + nmb - 10;
	}
	else
	{
		ft_uitoa_base_r(nmb / base, base, base_case, mem);
		ft_uitoa_base_r(nmb % base, base, base_case, mem);
	}
}

/*
**	base_case - upper case or lower case
*/

char		*ft_uitoa_base(const uintmax_t nmb, const unsigned int base,
							const char base_case)
{
	char			*rt;
	unsigned int	dg_am;
	uintmax_t		nmb_cp;

	dg_am = 1;
	nmb_cp = nmb;
	while (nmb_cp != 0 && dg_am++)
		nmb_cp = (nmb_cp < base) ? 0 : nmb_cp / base;
	if ((rt = ft_strnew(dg_am)))
		ft_uitoa_base_r(nmb, base, base_case, rt);
	return (rt);
}

char		*ft_uitoa(const uintmax_t nmb)
{
	return (ft_uitoa_base(nmb, 10, 'a'));
}

char		*ft_itoa_base(const intmax_t nmb, const unsigned int base,
							const char base_case)
{
	char	*rt;
	char	*temp;

	rt = ft_uitoa_base(ABS(nmb), base, base_case);
	if (nmb < 0)
	{
		temp = ft_strjoin("-", rt);
		ft_strdel(&rt);
		rt = temp;
	}
	return (rt);
}

char		*ft_itoa(const intmax_t nmb)
{
	return (ft_itoa_base(nmb, 10, 'a'));
}
