/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 11:18:43 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:33:57 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	**ft_strsplit(char const *const str, const char delimiter)
{
	char			**rt;
	size_t			ct;
	size_t			start;
	size_t			words_am;
	unsigned int	words_ct;

	if (str == NULL || !(words_am = (ft_strwrd_am(str, delimiter) + 1))
	|| !(rt = (char**)ft_memalloc(sizeof(char*) * words_am)))
		return (NULL);
	ct = 0;
	words_ct = 0;
	start = 0;
	while (words_ct < words_am - 1 && str[ct] != '\0')
	{
		while (str[ct] == delimiter)
			ct++;
		if (str[ct])
			start = ct;
		while (str[ct] != '\0' && str[ct] != delimiter)
			ct++;
		if (str[start])
			rt[words_ct++] = ft_strsub(str, start, ct - start);
	}
	return (rt);
}

char	*ft_strtrim(char const *const str)
{
	size_t	start;
	size_t	end;

	if (str == NULL)
		return (ft_strnew(0));
	start = 0;
	while (str[start] && ft_isspc(str[start]))
		start++;
	end = ft_strlen(str) - 1;
	if (start > end)
		return (ft_strnew(0));
	while (str[end] && ft_isspc(str[end]))
		end--;
	return (ft_strsub(str, start, end - start + 1));
}
