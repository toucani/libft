/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strAdvanced.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 16:53:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 21:46:51 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libft.h>
#include <algorithm>
#include <gtest/gtest.h>
#include "dummyData.hpp"

TEST(string, chrRchr)
{
    const char look4 = 'e';

    for (const auto& item : strings)
    {
        if (item.empty())
            continue;

        const auto * foundChar = ft_strchr(item.c_str(), look4);
        const auto foundPos = item.find(look4);
        EXPECT_EQ(foundPos, foundChar - item.c_str());

        const auto * foundRChar = ft_strrchr(item.c_str(), look4);
        const auto foundRPos = item.find_last_of(look4);
        EXPECT_EQ(foundRPos, foundRChar - item.c_str());
    }
}

TEST(string, join)
{
    for (const auto& item : strings)
    {
        EXPECT_EQ(std::string(ft_strjoin(nullptr, nullptr)), "");
        EXPECT_EQ(std::string(ft_strjoin(item.c_str(), nullptr)), item);
        EXPECT_EQ(std::string(ft_strjoin(item.c_str(), item.c_str())),
                  item + item);
        EXPECT_EQ(std::string(ft_strjoin_f(item.c_str(), item.c_str(),
                                           nullptr, nullptr)),
                  item + item);
        EXPECT_EQ(std::string(ft_strjoin_f(item.c_str(), item.c_str(),
                                           item.c_str(), nullptr)),
                  item + item + item);
        EXPECT_EQ(std::string(ft_strjoin_f(item.c_str(), item.c_str(),
                                           nullptr, item.c_str())),
                  item + item + item);
        EXPECT_EQ(std::string(ft_strjoin_f(item.c_str(), item.c_str(),
                                           item.c_str(), item.c_str())),
                  item + item + item + item);
    }
}

TEST(string, str)
{
    const std::string notFound = "u shall not find it!";
    for (const auto& item : strings)
    {
        if (item.find(notFound) == item.npos)
        {
            EXPECT_EQ(ft_strstr(item.c_str(), notFound.c_str()), nullptr);
            EXPECT_EQ(ft_strnstr(item.c_str(), notFound.c_str(),
                                 item.length() / 2), nullptr);
        }
        else
        {
            EXPECT_EQ(std::string(ft_strstr(item.c_str(), notFound.c_str())),
                      item.substr(item.find(notFound)));
            EXPECT_EQ(std::string(ft_strnstr(item.c_str(), notFound.c_str(),
                                             item.find(notFound) + 1)),
                      item.substr(item.find(notFound)));
        }

        if (item.length() <= 5)
            continue;

        const std::string found = item.substr(5, std::min<std::string::size_type>
                                                        (10, item.length()));
        EXPECT_EQ(std::string(ft_strstr(item.c_str(), found.c_str())),
                  item.substr(item.find(found)));
        EXPECT_EQ(std::string(ft_strnstr(item.c_str(), found.c_str(), 6)),
                  item.substr(item.find(found, 5)));
    }
}

TEST(string, split)
{
    const char delim = 'e';
    for (const auto& item : strings)
    {
        std::vector<std::string> words;
        std::stringstream ss(item);
        std::string token;
        while (std::getline(ss, token, delim))
        {
            if (token.empty())
                continue;
            words.emplace_back(token);
        }

        std::vector<std::string> resultWords;
        for (char **word = ft_strsplit(item.c_str(), delim);
             *word != nullptr; word++)
        {
            resultWords.emplace_back(*word);
        }

        EXPECT_EQ(words, resultWords);
    }
}

#define STRMATCH_TEST_TRUE(x, y)\
    EXPECT_TRUE(ft_strmatch(x, y));\
    EXPECT_TRUE(ft_strmatch(y, x));

#define STRMATCH_TEST_FALSE(x, y)\
    EXPECT_FALSE(ft_strmatch(x, y));\
    EXPECT_FALSE(ft_strmatch(y, x));

TEST(string, matchSimple)
{
    STRMATCH_TEST_TRUE(nullptr, nullptr);
    STRMATCH_TEST_FALSE("", nullptr);
    STRMATCH_TEST_FALSE("d", nullptr);
    STRMATCH_TEST_FALSE("?", nullptr);
    STRMATCH_TEST_FALSE("*", nullptr);
    STRMATCH_TEST_TRUE("", "");
    STRMATCH_TEST_TRUE("abcde", "abcde");
    STRMATCH_TEST_FALSE("abcde", "abcd");
    STRMATCH_TEST_FALSE("", "abcd");
}

TEST(string, matchAsterics)
{
    STRMATCH_TEST_TRUE("abc*", "abcde");
    STRMATCH_TEST_TRUE("*", "abcde");
    STRMATCH_TEST_TRUE("ABc*", "ABcde");
    STRMATCH_TEST_TRUE("*", "AbCde");
    STRMATCH_TEST_TRUE("*stR", "This is a stR");
    STRMATCH_TEST_TRUE("*stR*", "This is a stR");
    STRMATCH_TEST_TRUE("*stR*", "This is a stRing !");
    STRMATCH_TEST_TRUE("**", "This is a stRing");
    STRMATCH_TEST_FALSE("Str*ng!", "This is a String!String!");
    STRMATCH_TEST_TRUE("*Str*ng!", "This is a String!String!");
    STRMATCH_TEST_TRUE("This is a Str*ng!", "This is a String!String!");
    STRMATCH_TEST_FALSE("ma* me", "match me to * string");
    STRMATCH_TEST_FALSE("ma* me", "match*to this string");
    STRMATCH_TEST_TRUE("ma* me*", "match me to * string");
}

TEST(string, matchQMark)
{
    STRMATCH_TEST_TRUE("A?Cde", "AbCde");
    STRMATCH_TEST_FALSE("?", "AbCde");
    STRMATCH_TEST_FALSE("?", "");
    STRMATCH_TEST_TRUE("?", "?");
}

TEST(string, matchMisc)
{
    STRMATCH_TEST_TRUE("A?C*", "AbCde");
    STRMATCH_TEST_TRUE("*stR?ng", "This is a stRing");
    STRMATCH_TEST_TRUE("*stR?*", "This is a stRing !");
    STRMATCH_TEST_TRUE("*stR?*", "This is a stRi");
    STRMATCH_TEST_FALSE("*stR?*", "This is a stR");
    STRMATCH_TEST_FALSE("Str*ng!", "This is a ?tring!");
    STRMATCH_TEST_FALSE("*Str*ng!", "This is a ?tring!");
}
