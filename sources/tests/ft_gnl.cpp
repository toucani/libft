/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gnl.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 13:34:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 21:59:26 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <gtest/gtest.h>
#include "get_next_line.h"

TEST(gnl, basic)
{
    std::string theBookPath(SOURCE_PATH);
    theBookPath += "/sources/tests/theBook.txt";

    //C++ file reading
    std::ifstream theBook(theBookPath);

    EXPECT_TRUE(theBook.is_open());
    EXPECT_TRUE(theBook.good());

    //C file reading
    auto theBookFd = open(theBookPath.c_str(), O_RDONLY);
    EXPECT_TRUE(theBookFd >= 0);

    auto retVal = SCC_CODE;
    while (!theBook.eof() && retVal == SCC_CODE)
    {
        char *rawCLine = nullptr;
        retVal = get_next_line(theBookFd, &rawCLine);
        EXPECT_NE(retVal, ERR_CODE);
        std::string cppLine, cLine(rawCLine);
        std::getline(theBook, cppLine);

        EXPECT_EQ(cppLine, cLine);
    }
}
