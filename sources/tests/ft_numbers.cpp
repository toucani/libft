/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_numbers.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 20:30:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/22 21:48:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libft.h>
#include <gtest/gtest.h>
#include "dummyData.hpp"

TEST(itoa, decimal)
{
    for (const auto &item : numbers)
    {
        EXPECT_EQ(std::string(ft_itoa(item)), std::to_string(item));
        EXPECT_EQ(std::string(ft_itoa_base(item, 10, 'a')), std::to_string(item));
        EXPECT_EQ(std::string(ft_itoa_base(item, 10, 'A')), std::to_string(item));
    }
}

TEST(itoa, advanced)
{
    char octal[255];
    char hexa[255];

    for (const auto &item : numbers)
    {
        if (item < 0)
            continue;

        memset(octal, 0, 255);
        memset(hexa, 0, 255);

        std::snprintf(octal, 255, "%lo", item);
        EXPECT_EQ(std::string(ft_uitoa_base(item, 8, 'a')), std::string(octal));

        std::snprintf(hexa, 255, "%lx", item);
        EXPECT_EQ(std::string(ft_uitoa_base(item, 16, 'a')), std::string(hexa));

        memset(hexa, 0, 255);
        std::snprintf(hexa, 255, "%lX", item);
        EXPECT_EQ(std::string(ft_uitoa_base(item, 16, 'A')), std::string(hexa));
    }
}

TEST(swap, simple)
{
    int a = 5, b = 2;

    EXPECT_EQ(a, 5);
    EXPECT_EQ(b, 2);

    SWAP(&a, &b);

    EXPECT_EQ(a, 2);
    EXPECT_EQ(b, 5);

    SWAP(&a, &b);
    EXPECT_EQ(a, 5);
    EXPECT_EQ(b, 2);
}

#define ATOI_TEST(x)\
    EXPECT_EQ(ft_atoi(x), std::atoi(x));

TEST(atoi, simple)
{
    ATOI_TEST("145");
    ATOI_TEST("+145");
    ATOI_TEST("-145");
    EXPECT_EQ(ft_atoi(nullptr), 0);//std::atoi crashes in this case
    ATOI_TEST("");
    ATOI_TEST("0");
}

TEST(atoi, advanced)
{
    ATOI_TEST("                         3004");
    ATOI_TEST("           +             3004");
    ATOI_TEST("           -             3004");
    ATOI_TEST("3004This text is really hidden");
    ATOI_TEST("               3004Hidden text");
    ATOI_TEST("Just some text");
    ATOI_TEST("-Just some text");
    ATOI_TEST("-");
    ATOI_TEST("+");
    ATOI_TEST("---");
    ATOI_TEST("-+-");
    ATOI_TEST("+-+");
    ATOI_TEST("+++");
    ATOI_TEST("---76");
    ATOI_TEST("+++76");
    ATOI_TEST("2147483647");
    ATOI_TEST("+2147483647");
    ATOI_TEST("-2147483647");
}
