/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dummyData.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 17:21:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/01/06 17:21:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <vector>

const std::vector<std::string> strings =
{
    "",
    "Lorem ipsum dolor sit amet, ludus affert melius eos an,",
    "ei pro ludus veniam reformidans,\0quod assueverit id mel.",
    "Eam ei paulo sapientem, eam erat ridens an. Ei vide inciderint"
        "mea. Ei vim eruditi postulant, ut oportere repudiandae"
        "est. Has euismod sanctus at, ad lorem verear eam.",
    "Molestie menandri his cu. Mazim congue id eam, vis ne regione "
        "debitis deseruisse. Hinc adolescens eu vis, his et tale "
        "etiam ceteros, ius prompta denique an. Nobis eirmod nostrud "
        "in ius, putent integre eleifend ex vix, in eos nemore forensibus."
        " Ne agam mutat aeque vix, sea ad tota adipisci contentiones. "
        "No sed maluisset abhorreant, suas novum aperiam mea ea, has "
        "in eripuit fastidii."
};

const std::vector<std::basic_string<wchar_t>> wideStrings =
{
    L"",
    L"Lorem ipsum dolor sit amet, ludus affert melius eos an,",
    L"ei pro ludus veniam reformidans,\0quod assueverit id mel.",
    L"Eam ei paulo sapientem, eam erat ridens an. Ei vide inciderint"
        "mea. Ei vim eruditi postulant, ut oportere repudiandae"
        "est. Has euismod sanctus at, ad lorem verear eam.",
    L"Molestie menandri his cu. Mazim congue id eam, vis ne regione "
        "debitis deseruisse. Hinc adolescens eu vis, his et tale "
        "etiam ceteros, ius prompta denique an. Nobis eirmod nostrud "
        "in ius, putent integre eleifend ex vix, in eos nemore forensibus."
        " Ne agam mutat aeque vix, sea ad tota adipisci contentiones. "
        "No sed maluisset abhorreant, suas novum aperiam mea ea, has "
        "in eripuit fastidii."
};

const std::vector<long> numbers =
{
    0, -1, 1, 45, -45, 123456789, -123456789, 2147483650, -2147483650,
    -148972902, -66844587, -380981615, 111632690, 328795388, -161532344,
    -355843005, -948567814, 606862265, 937728302, 20998602, -555035622,
    -960869672, 460271306, -875579925, 419806513, -423172085, -335799014,
    615575168, 96823259, -994913049, 912959732, 677069633, 246536160,
    727737629, -699129956, 800989611, 212458417, 726313099, 12307711,
    -610797815, -678025697, -149206793, 399960218, -256238881, 384878935,
    181183844, -722722350, 252460515, 922190897, -206693787, 869685711,
    113124515, -496450159, 550359713, 121311322, 820591408, -642130916,
    -703695497, -624370949,
};