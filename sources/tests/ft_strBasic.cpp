/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strBasic.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 16:53:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 21:46:59 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libft.h>
#include <gtest/gtest.h>
#include "dummyData.hpp"

TEST(string, length)
{
    for (const auto& item : strings)
    {
        EXPECT_EQ(ft_strlen(item.c_str()), item.length());
        if (item.empty())
            continue;
        EXPECT_EQ(ft_strlen_dl(item.c_str(), ' '), item.find_first_of(' '));
    }
}

TEST(string, compareBasic)
{
    for (size_t ct = 0; ct < strings.size() - (strings.size() % 2); ct += 2)
    {
        std::pair<bool, bool> control = std::make_pair(
                strings[ct] == strings[ct],
                strings[ct] == strings[ct + 1]);
        std::pair<bool, bool> cmpResult = std::make_pair(
                ft_strcmp(strings[ct].c_str(), strings[ct].c_str()) == 0,
                ft_strcmp(strings[ct].c_str(), strings[ct + 1].c_str()) == 0);
        std::pair<bool, bool> equResult = std::make_pair(
                ft_strequ(strings[ct].c_str(), strings[ct].c_str()),
                ft_strequ(strings[ct].c_str(), strings[ct + 1].c_str()));
        EXPECT_EQ(control, cmpResult);
        EXPECT_EQ(control, equResult);
    }
}

TEST(string, compareAdvanced)
{
    auto stringsWithTails = strings;
    for (auto & item : stringsWithTails)
    {
        item += " -> this is the tail, which must not be compared.";
    }

    for (size_t ct = 0; ct < strings.size(); ct++)
    {
        EXPECT_TRUE(ft_strncmp(strings[ct].c_str(),
                               stringsWithTails[ct].c_str(),
                               strings[ct].length()) == 0);
        EXPECT_FALSE(ft_strncmp(strings[ct].c_str(),
                               stringsWithTails[ct].c_str(),
                               stringsWithTails[ct].length()) == 0);

        EXPECT_TRUE(ft_strnequ(strings[ct].c_str(),
                               stringsWithTails[ct].c_str(),
                               strings[ct].length()));
        EXPECT_FALSE(ft_strnequ(strings[ct].c_str(),
                               stringsWithTails[ct].c_str(),
                               stringsWithTails[ct].length()));
    }
}

TEST(string, clr)
{
    for (const auto& item : strings)
    {
        auto * copy = ft_strdup(item.c_str());
        ft_strclr(copy);
        EXPECT_TRUE(std::string(copy).empty());
    }
}

TEST(string, copy)
{
    for (const auto& item : strings)
    {
        EXPECT_EQ(std::string(ft_strdup(item.c_str())), item);
        const auto * subStr = ft_strsub(item.c_str(), 0, 6);
        EXPECT_EQ(std::string(subStr), item.substr(0, 6));

        char * copyStr = ft_strnew(item.length());
        char * copynStr = ft_strnew(item.length());

        EXPECT_EQ(std::string(ft_strcpy(copyStr, item.c_str())), item);
        EXPECT_EQ(std::string(
                ft_strncpy(copynStr, item.c_str(), std::min(item.length(),
                        static_cast<std::string::size_type>(10)))),
                item.substr(0, std::min(item.length(), static_cast<std::string::size_type>(10))));
    }
}

TEST(string, concat)
{
    const std::string tail = "this is a dummy tail";

    for (const auto& item : strings)
    {
        char * str = ft_strnew(item.size() + tail.size());
        ft_strcat(str, item.c_str());
        ft_strcat(str, tail.c_str());
        EXPECT_EQ(std::string(str), item + tail);

        char * nstr = ft_strnew(item.size() + tail.size());
        ft_strcat(nstr, item.c_str());
        ft_strcat(nstr, tail.substr(0, 5).c_str());
        EXPECT_EQ(std::string(nstr), item + tail.substr(0, 5));
    }
}
