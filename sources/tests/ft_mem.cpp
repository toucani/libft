/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 22:20:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 21:46:37 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libft.h>
#include <gtest/gtest.h>

const auto testDataLength = 4096; //Must not be less than 23

TEST(mem, bzero)
{
    char *mem1 = new char[testDataLength];

    const auto TestIt = [&](const char ch)
    {
        ft_memset(mem1, ch, testDataLength);
        ft_bzero(mem1, testDataLength);
        for (auto ct = 0; ct < testDataLength; ct++)
        {
            EXPECT_EQ(mem1[ct], 0);
        }
    };

    TestIt('^');
    TestIt('@');

    delete[] mem1;
}

TEST(mem, memset)
{
    char *mem1 = new char[testDataLength];

    const auto TestIt = [&](const char ch)
    {
        ft_bzero(mem1, testDataLength);
        ft_memset(mem1, ch, testDataLength);
        for (auto ct = 0; ct < testDataLength; ct++)
        {
            EXPECT_EQ(mem1[ct], ch);
        }
    };

    TestIt('^');
    TestIt('@');

    delete[] mem1;
}

TEST(mem, compare)
{
    char *mem1 = new char[testDataLength];
    char *mem2 = new char[testDataLength];

    ft_memset(mem1, '&', testDataLength);
    ft_memset(mem2, '&', testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), 0);

    mem2[testDataLength - 2] = '3';

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), '&' - '3');

    mem1[testDataLength - 3] = '`';

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), '`' - '&');

    mem1[testDataLength - 4] = '!';
    mem2[testDataLength - 4] = '#';

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), '!' - '#');

    delete[] mem1;
    delete[] mem2;
}

TEST(mem, copy)
{
    char *mem1 = new char[testDataLength];
    char *mem2 = new char[testDataLength];

    ft_memset(mem1, '&', testDataLength);
    ft_memset(mem2, '-', testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), '&' - '-');

    ft_memcpy(mem2, mem1, testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), 0);

    ft_memset(&mem1[5], '+', 5);
    ft_memcpy(mem2, mem1, testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), 0);

    mem1[10] = 'x';

    ft_memccpy(mem2, mem1, 'x', testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), 0);

    delete[] mem1;
    delete[] mem2;
}

TEST(mem, move)
{
    char *mem1 = new char[testDataLength];
    char *mem2 = new char[testDataLength];

    ft_bzero(mem1, testDataLength);
    ft_bzero(mem2, testDataLength);
    ft_memcpy(mem2, "...This is a string...", 22);
    ft_memmove(mem1, mem2, testDataLength);

    EXPECT_EQ(ft_memcmp(mem1, mem2, testDataLength), 0);

    ft_memmove(mem1, &(mem1[3]), testDataLength - 4);

    EXPECT_EQ(ft_memcmp(mem1, &mem2[3], testDataLength - 4), 0);

    ft_memmove(&(mem1[5]), mem1, testDataLength - 6);

    EXPECT_EQ(ft_memcmp(mem1, &mem2[3], 4), 0);
    EXPECT_EQ(ft_memcmp(&mem1[5], &mem2[3], testDataLength - 9), 0);
}
