/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstr.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/06 16:21:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 21:47:08 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <libft.h>
#include <gtest/gtest.h>
#include "dummyData.hpp"

TEST(wideString, length)
{
    for(const auto& item : wideStrings)
    {
        EXPECT_EQ(ft_wstrlen(item.c_str()), item.length());
    }
}

TEST(wideString, dup)
{
    for(const auto& item : wideStrings)
    {
        EXPECT_EQ(std::basic_string<wchar_t>(ft_wstrdup(item.c_str())), item);
    }
}
