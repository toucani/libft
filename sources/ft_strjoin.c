/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 14:24:15 by dkovalch          #+#    #+#             */
/*   Updated: 2017/07/23 19:47:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	*ft_strjoin(char const *const str1, char const *const str2)
{
	char	*rt;

	if (!(rt = ft_strnew(ft_strlen(str1) + ft_strlen(str2))))
		return (NULL);
	ft_strcat(rt, str1);
	ft_strcat(rt, str2);
	return (rt);
}

char	*ft_strjoin_del_first(char **str1, char const *const str2)
{
	char	*rt;

	rt = ft_strjoin(*str1, str2);
	ft_strdel(str1);
	return (rt);
}

char	*ft_strjoin_ultimate(char **str1, char **str2)
{
	char	*rt;

	rt = ft_strjoin(*str1, *str2);
	ft_strdel(str1);
	ft_strdel(str2);
	return (rt);
}

char	*ft_strjoin_f(const char *const str1, const char *const str2,
						const char *const str3, const char *const str4)
{
	char	*rt[2];

	rt[0] = ft_strjoin(str1, str2);
	rt[1] = ft_strjoin(str3, str4);
	return (ft_strjoin_ultimate(&rt[0], &rt[1]));
}
