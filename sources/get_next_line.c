/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/09 14:54:49 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 11:56:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"
#include <unistd.h>
#include "get_next_line.h"
#include "get_next_line_list.h"

/*
**	Getting the list element with the necessary fd, or creating one.
*/

static t_gnl_list	*get_the_elem(const int fd, t_gnl_list **the_list)
{
	t_gnl_list	*elem;

	elem = *the_list;
	while (elem && elem->fd != fd)
		elem = elem->next;
	if (!elem)
	{
		elem = ft_memalloc(sizeof(t_gnl_list));
		elem->next = *the_list;
		elem->fd = fd;
		elem->line = ft_strnew(0);
		*the_list = elem;
	}
	return (elem);
}

/*
**	Reading the line.
*/

static void			read_next_line(t_gnl_list *elem)
{
	char	*part;
	ssize_t	smb_am;

	part = ft_strnew(BUFF_SIZE);
	while (!ft_strchr(elem->line, '\n') &&
		(smb_am = read(elem->fd, part, BUFF_SIZE)))
	{
		part[smb_am] = '\0';
		elem->line = ft_strjoin_del_first(&(elem->line), part);
	}
	ft_strdel(&part);
}

/*
**	Writing into the <line> variable and deleting cut stuff.
*/

static int			create_next_line(t_gnl_list *elem, char **line)
{
	char	*eol;
	char	*temp;

	if (ft_strlen(elem->line) == 0)
		return (END_CODE);
	eol = ft_strchr(elem->line, '\n');
	*line = ft_strsub(elem->line, 0,
		(eol) ? (size_t)(eol - elem->line) : ft_strlen(elem->line));
	(eol) ? eol++ : 0;
	temp = ft_strsub(eol, 0, ft_strlen(eol));
	ft_strdel(&(elem->line));
	elem->line = temp;
	return (SCC_CODE);
}

int					get_next_line(const int fd, char **line)
{
	static t_gnl_list	*the_list = 0;
	t_gnl_list			*current_elem;
	char				temp;

	if (read(fd, &temp, 0) < 0)
		return (ERR_CODE);
	current_elem = get_the_elem(fd, &the_list);
	read_next_line(current_elem);
	return (create_next_line(current_elem, line));
}
