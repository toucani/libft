/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 12:40:51 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:34:12 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	*ft_strstr(const char *const str, const char *const to_find)
{
	return (ft_strnstr(str, to_find, MAX(ft_strlen(str), ft_strlen(to_find))));
}

char	*ft_strnstr(const char *const str, const char *const to_find,
					const size_t len)
{
	size_t	ct;

	if (to_find[0] == '\0')
		return ((char*)str);
	ct = 0;
	while (str != NULL && str[ct] != '\0' && ct < len)
	{
		if (str[ct] == to_find[0])
			if (ft_strnequ(&str[ct], to_find, ft_strlen(to_find)))
				return ((char*)(&str[ct]));
		ct++;
	}
	return (NULL);
}
