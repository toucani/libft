/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 16:35:49 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:29:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

size_t	ft_strlen(const char *const str)
{
	size_t rt;

	rt = 0;
	while (str != NULL && str[rt] != '\0')
		rt++;
	return (rt);
}

/*
**	Counting the length of the string to the delimiter(or to the end)
*/

size_t	ft_strlen_dl(const char *const str, const char delimiter)
{
	char	*rt;

	if (str != NULL && (rt = ft_strchr(str, delimiter)))
		return (rt - str);
	return (ft_strlen(str));
}

/*
**	Counting amount of "words"(strings between delimiters)
*/

size_t	ft_strwrd_am(const char *const str, const char delimiter)
{
	size_t	ct;
	size_t	am;

	ct = 0;
	am = 0;
	while (str != NULL && str[ct] != '\0')
	{
		while (str[ct] == delimiter)
			ct++;
		if (str[ct] != '\0' && str[ct] != delimiter)
			am++;
		while (str[ct] != '\0' && str[ct] != delimiter)
			ct++;
	}
	return (am);
}
