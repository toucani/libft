/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 12:59:00 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 17:56:34 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

ssize_t	ft_putstr_fd(char const *const str, const int fd)
{
	return (write(fd, str, ft_strlen(str) * sizeof(char)));
}

ssize_t	ft_putstr(char const *const str)
{
	return (ft_putstr_fd(str, STDOUT_FILENO));
}

ssize_t	ft_putendl_fd(char const *const str, const int fd)
{
	return (ft_putstr_fd(str, fd) + ft_putstr_fd("\n", fd));
}

ssize_t	ft_putendl(char const *const str)
{
	return (ft_putstr(str) + ft_putstr("\n"));
}
