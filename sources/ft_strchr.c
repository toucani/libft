/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/07 11:54:56 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:38:56 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	*ft_strchr(const char *const str, const char ch)
{
	return ((char*)ft_memchr((const void*)str, ch, ft_strlen(str)));
}

char	*ft_strrchr(const char *const str, const char ch)
{
	int	ct;

	ct = ft_strlen(str) + 1;
	if (ch == '\0' && str != NULL)
		return ((char*)(&(str[ct - 1])));
	while (str != NULL && --ct >= 0)
		if (str[ct] == ch)
			return ((char*)(&str[ct]));
	return (NULL);
}
