/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 15:32:25 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:26:40 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

int		ft_strcmp(const char *const str1, const char *const str2)
{
	return (ft_strncmp(str1, str2, MAX(ft_strlen(str1), ft_strlen(str2))));
}

int		ft_strncmp(const char *const str1, const char *const str2,
					const size_t len)
{
	return (ft_memcmp((const void*)str1, (const void*)str2, len));
}

bool	ft_strequ(const char *const str1, const char *const str2)
{
	return (ft_strcmp(str1, str2) == 0);
}

bool	ft_strnequ(const char *const str1, const char *const str2,
					const size_t len)
{
	return (ft_strncmp(str1, str2, len) == 0);
}
