/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmatch.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/08 13:24:46 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/24 16:09:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

/*
**	Ft_strmatch matches wildcars and question marks in both strings
*/

/*
**	Function wild_action works with a wildcard only in the first string!
*/

static bool	wild_action(const char *str1, const char *str2)
{
	char	*temp;
	ssize_t	qmarks_am;

	qmarks_am = 0;
	while (*str1 == '*' || *str1 == '?')
	{
		qmarks_am += (*str1 == '?') ? 1 : 0;
		str1++;
	}
	if (*str1 == '\0' && qmarks_am == 0)
		return (true);
	temp = ft_strchr(str2, *str1);
	while (temp && *temp && ABS(temp - str2) < qmarks_am)
		temp = ft_strchr(temp + 1, *str1);
	while (temp && ABS(temp - str2) >= qmarks_am && !ft_strmatch(str1, temp))
		temp = ft_strchr(temp + 1, *str1);
	return (temp != NULL);
}

static bool	take_action(const char *str1, const char *str2)
{
	ssize_t	len;

	if (*str1 == '?' || *str2 == '?')
		return (ft_strmatch(str1 + 1, str2 + 1));
	if (*str1 == '*')
		return (wild_action(str1, str2));
	if (*str2 == '*')
		return (wild_action(str2, str1));
	len = ABS(ft_strchr(str1, '*') - str1);
	len = (ABS(ft_strchr(str1, '?') - str1) < len) ?
			ABS(ft_strchr(str1, '?') - str1) : len;
	len = (ABS(ft_strchr(str2, '*') - str2) < len) ?
			ABS(ft_strchr(str2, '*') - str2) : len;
	len = (ABS(ft_strchr(str2, '?') - str2) < len) ?
			ABS(ft_strchr(str2, '?') - str2) : len;
	return (ft_strnequ(str1, str2, len) && ft_strmatch(str1 + len, str2 + len));
}

bool		ft_strmatch(const char *str1, const char *str2)
{
	if ((str1 == NULL) ^ (str2 == NULL))
		return (false);
	if (!ft_strchr(str1, '*') && !ft_strchr(str2, '*') &&
		!ft_strchr(str1, '?') && !ft_strchr(str2, '?'))
		return (ft_strequ(str1, str2));
	if (ft_strequ(str1, "*") || ft_strequ(str2, "*") ||
		(*str1 == '\0' && *str2 == '\0'))
		return (true);
	if (*str1 == '\0' || *str2 == '\0')
		return (false);
	return (take_action(str1, str2));
}
