/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 10:48:16 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:28:23 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

char	*ft_strcpy(char *const dest, const char *const src)
{
	return (ft_strncpy(dest, src, ft_strlen(src) + 1));
}

char	*ft_strncpy(char *const dest, const char *const src, const size_t len)
{
	return (ft_memmove((void*)dest, (void*)src, len));
}

char	*ft_strcat(char *const dest, const char *const src)
{
	return (ft_strcpy(&(dest[ft_strlen(dest)]), src));
}

char	*ft_strncat(char *const dest, const char *const src, const size_t len)
{
	return (ft_strncpy(&(dest[ft_strlen(dest)]), src,
	MIN(len, ft_strlen(src) + 1)));
}
