/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mem2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 16:50:35 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/16 20:20:47 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

void	*ft_memcpy(void *const dst, const void *const src, const size_t len)
{
	size_t ct;

	ct = 0;
	while (dst != NULL && src != NULL && ct < len)
	{
		((unsigned char*)dst)[ct] = ((unsigned char*)src)[ct];
		ct++;
	}
	return (dst);
}

void	*ft_memccpy(void *const dst, const void *const src, const char ch,
					const size_t len)
{
	size_t	ct;

	ct = 0;
	while (dst != NULL && src != NULL && ct < len)
	{
		((unsigned char*)dst)[ct] = ((unsigned char*)src)[ct];
		if (((unsigned char*)dst)[ct] == (unsigned char)ch)
			return ((void*)&((unsigned char*)dst)[ct + 1]);
		ct++;
	}
	return (NULL);
}

void	*ft_memmove(void *const dst, const void *const src, const size_t len)
{
	size_t	ct;

	ct = len;
	if (dst == NULL || src == NULL)
		return (dst);
	if (dst - src < 0)
		return (ft_memcpy(dst, src, len));
	else if (src - dst < 0)
		while (ct-- > 0)
			((unsigned char*)dst)[ct] = ((unsigned char*)src)[ct];
	return (dst);
}

int		ft_memcmp(const void *const mem1, const void *const mem2,
					const size_t len)
{
	size_t ct;

	ct = 0;
	while (mem1 != NULL && mem2 != NULL && ct < len
		&& ((unsigned char*)mem1)[ct] == ((unsigned char*)mem2)[ct])
		ct++;
	if (ct == len || mem1 == NULL || mem2 == NULL)
		return (0);
	else
		return (((unsigned char*)mem1)[ct] - ((unsigned char*)mem2)[ct]);
}

void	*ft_memchr(const void *const str, const char ch, const size_t len)
{
	size_t ct;

	ct = 0;
	while (str != NULL && ct < len)
	{
		if (((unsigned char*)str)[ct] == (unsigned char)ch)
			return ((void*)&(((unsigned char*)str)[ct]));
		ct++;
	}
	return (NULL);
}
