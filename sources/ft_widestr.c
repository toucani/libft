/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_widestr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/01 12:22:55 by dkovalch          #+#    #+#             */
/*   Updated: 2017/09/17 18:02:43 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "libft.h"

wchar_t	*ft_wstrnew(const size_t amount)
{
	return ((wchar_t*)ft_memalloc(sizeof(wchar_t) * (amount + 1)));
}

void	ft_wstrdel(wchar_t **str)
{
	ft_memdel((void**)str);
}

size_t	ft_wstrlen(const wchar_t *const str)
{
	size_t ct;

	ct = 0;
	while (str != NULL && str[ct] != '\0')
		ct++;
	return (ct);
}

wchar_t	*ft_wstrdup(const wchar_t *const str)
{
	wchar_t *rt;

	rt = NULL;
	if (str != NULL && (rt = ft_wstrnew(ft_wstrlen(str))))
		rt = (wchar_t *)ft_memcpy(rt, str, sizeof(wchar_t) * ft_wstrlen(str));
	return (rt);
}
