/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 17:14:41 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/18 11:41:57 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

/*
**	Change BUFF_SIZE to change the amount of read symbols.
**	GNL supports different file descriptors.
*/

# include <stdio.h>

# ifdef BUFSIZ
#  define BUFF_SIZE BUFSIZ
# else
#  define BUFF_SIZE 2048
# endif

# define ERR_CODE -1
# define END_CODE 0
# define SCC_CODE 1

# ifdef __cplusplus
 extern "C"{
# endif

int	get_next_line(int fd, char **line) __attribute__((warn_unused_result));

# ifdef __cplusplus
 }
# endif

#endif
