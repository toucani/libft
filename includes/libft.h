/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 12:56:54 by dkovalch          #+#    #+#             */
/*   Updated: 2018/08/22 21:46:18 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of libft project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stddef.h>
# include <stdlib.h>
# include <stdbool.h>
# include <inttypes.h>
# include "get_next_line.h"

# ifndef ABS
#  define ABS(x) (((x) < 0) ? -(x) : (x))
# endif

# ifndef MIN
#  define MIN(x, y) (((x) < (y)) ? (x) : (y))
# endif

# ifndef MAX
#  define MAX(x, y) (((x) > (y)) ? (x) : (y))
# endif

# ifndef NULL
#  define NULL ((void *) 0)
# endif

# ifndef STR
#  ifndef XSTR
#   define XSTR(x) #x
#  endif
#  define STR(x) XSTR(x)
# endif

# ifndef SWAP
#  define SWAP(x, y) *x ^= *y; *y ^= *x; *x ^= *y;
# endif

# define BIT_SET(x, y) ((x) & (1 << y))
# define BIT_UNSET(x, y) (!((x) & (1 << y)))

# ifdef __cplusplus
extern "C"{
# endif

/*
**	Matching functions.
*/

bool			ft_isspc(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_isalnum(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_isalpha(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_isascii(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_isdigit(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_isprint(const int
					letter) __attribute__((const,warn_unused_result));
bool			ft_strmatch(const char *str1,
					const char *str2) __attribute__((pure,warn_unused_result));

/*
**	Memory working functions.
*/

void			*ft_memalloc(const size_t
					size) __attribute__((malloc,warn_unused_result));
void			*ft_memset(void *const dest, const char ch, const size_t len);
void			*ft_memchr(const void *const str, const char ch,
					const size_t len) __attribute__((pure,warn_unused_result));
void			*ft_memmove(void *const dst, const void *const src,
							const size_t len);
void			*ft_memcpy(void *const dst, const void *const src,
							const size_t len);
void			*ft_memccpy(void *const dst, const void *const src,
							const char ch, const size_t len);
void			ft_memdel(void **ap);
void			ft_bzero(void *dst, const size_t len);
int				ft_memcmp(const void *const mem1, const void *const mem2,
					const size_t len) __attribute__((pure,warn_unused_result));

/*
**	Wide string functions
*/

wchar_t			*ft_wstrnew(const size_t
					amount) __attribute__((malloc,warn_unused_result));
void			ft_wstrdel(wchar_t **str);
wchar_t			*ft_wstrdup(const wchar_t
					*const str) __attribute__((malloc,warn_unused_result));
size_t			ft_wstrlen(const wchar_t
					*const str) __attribute__((pure,warn_unused_result));

/*
**	String working functions
*/

int				ft_strcmp(const char *const str1, const char
					*const str2) __attribute__((pure,warn_unused_result));
bool			ft_strequ(char const *const str1, char const
					*const str2) __attribute__((pure,warn_unused_result));
int				ft_strncmp(const char *const str1, const char *const str2,
					const size_t len) __attribute__((pure,warn_unused_result));
bool			ft_strnequ(const char *const str1, const char *const str2,
					const size_t len) __attribute__((pure,warn_unused_result));
char			**ft_strsplit(char const *const str, const char
					delimiter) __attribute__((malloc,warn_unused_result));
char			*ft_strcpy(char *const dest, const char *const src);
char			*ft_strncpy(char *const dest, const char *const src,
							const size_t len);
char			*ft_strcat(char *const dest, const char *const src);
char			*ft_strncat(char *const dest, const char *const src,
							const size_t len);
char			*ft_strchr(const char *const str, const
					char ch) __attribute__((pure,warn_unused_result));
char			*ft_strrchr(const char *const str, const
					char ch) __attribute__((pure,warn_unused_result));
char			*ft_strnew(size_t
					size) __attribute__((malloc,warn_unused_result));
char			*ft_strdup(const char *const
					str) __attribute__((malloc,warn_unused_result));
char			*ft_strsub(char const *const str, const size_t start, const
					size_t len) __attribute__((malloc,warn_unused_result));
void			ft_strdel(char **as);
void			ft_strclr(char *const str);
char			*ft_strnstr(const char *const str, const char *const to_find,
					const size_t len) __attribute__((pure,warn_unused_result));
char			*ft_strstr(const char *const str, const char *const
					to_find) __attribute__((pure,warn_unused_result));
char			*ft_strjoin(char const *const str1, char const *const
					str2) __attribute__((malloc,warn_unused_result));
char			*ft_strjoin_del_first(char **str1, char const *const
					str2) __attribute__((malloc,warn_unused_result));
char			*ft_strjoin_ultimate(char **str1, char
					**str2) __attribute__((malloc,warn_unused_result));
char			*ft_strjoin_f(const char *const str1, const char *const str2,
					const char *const str3, const char *const
					str4) __attribute__((malloc,warn_unused_result));
char			*ft_strtrim(char const *const
					str) __attribute__((malloc,warn_unused_result));
void			ft_strtolow(char *str);
void			ft_strtoup(char *str);
size_t			ft_strlen(const char *const
					str) __attribute__((pure,warn_unused_result));
size_t			ft_strlen_dl(const char *const str, const char
					delimiter) __attribute__((pure,warn_unused_result));
size_t			ft_strwrd_am(const char *const str, const char
					delimiter) __attribute__((pure,warn_unused_result));

/*
**	Numbers
*/

char			*ft_itoa(const intmax_t
						nmb) __attribute__((malloc,warn_unused_result));
char			*ft_uitoa(const uintmax_t
						nmb) __attribute__((malloc,warn_unused_result));
char			*ft_itoa_base(const intmax_t
							nmb, const unsigned int base,
					const char
					base_case) __attribute__((malloc,warn_unused_result));
char			*ft_uitoa_base(const uintmax_t nmb, const unsigned int base,
					const char
					base_case) __attribute__((malloc,warn_unused_result));
intmax_t		ft_atoi(const char *str) __attribute__((pure));

/*
**	Printing
*/

ssize_t			ft_putchar(const uint32_t symbol);
ssize_t			ft_putchar_fd(const uint32_t symbol, const int fd);
ssize_t			ft_putendl(char const *const str);
ssize_t			ft_putendl_fd(char const *const str, const int fd);
ssize_t			ft_putstr(char const *const str);
ssize_t			ft_putstr_fd(char const *const str, const int fd);
ssize_t			ft_putwstr(const wchar_t *const str);
ssize_t			ft_putwstr_fd(const wchar_t *const str, const int fd);
ssize_t			ft_putnbr(const intmax_t nmb);
ssize_t			ft_putnbr_fd(const intmax_t nmb, const int fd);

/*
**	Other stuff
*/

char			ft_tolower(const int
					letter) __attribute__((const,warn_unused_result));
char			ft_toupper(const int
					letter) __attribute__((const,warn_unused_result));

# ifdef __cplusplus
}
# endif

#endif
